<?php

namespace Reaktion\Tracking\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reaktion\Tracking\Model\Config;

/**
 * View model for tracking script
 */
class TrackingScript implements ArgumentInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }

    /**
     * Get tracking script url
     *
     * @return string
     */
    public function getTrackingScriptUrl()
    {
        $websiteId = (int) $this->storeManager->getStore()->getWebsiteId();
        return $this->config->getTrackingScriptUrl($websiteId);
    }
}
