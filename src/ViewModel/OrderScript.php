<?php

namespace Reaktion\Tracking\ViewModel;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Reaktion\Tracking\Model\Config;

/**
 * View model for order script
 */
class OrderScript implements ArgumentInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param CheckoutSession $checkoutSession
     * @param Config $config
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        Config $config,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Get order id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        $order = $this->getOrder();
        if (!$order) {
            return null;
        }
        return (int) $order->getEntityId();
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }

    /**
     * Get order
     *
     * @return OrderInterface|null
     */
    private function getOrder()
    {
        $lastOrderId = $this->checkoutSession->getLastOrderId();
        if (!$lastOrderId) {
            return null;
        }
        return $this->orderRepository->get((int) $lastOrderId);
    }
}
