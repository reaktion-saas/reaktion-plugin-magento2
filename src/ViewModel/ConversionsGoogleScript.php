<?php

namespace Reaktion\Tracking\ViewModel;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reaktion\Tracking\Model\Config;

/**
 * View model for conversions google script
 */
class ConversionsGoogleScript implements ArgumentInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param CheckoutSession $checkoutSession
     * @param Config $config
     * @param OrderRepositoryInterface $orderRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        Config $config,
        OrderRepositoryInterface $orderRepository,
        StoreManagerInterface $storeManager
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->orderRepository = $orderRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * Get conversions
     *
     * @return array with `currency`, `send_to`, `transaction_id` and `value` keys
     */
    public function getConversions()
    {
        $order = $this->getOrder();
        if (!$order) {
            return [];
        }
        return array_filter(
            array_map(function ($conversionConfig) use ($order) {
                if (!isset($conversionConfig['conversion_action_id'])) {
                    return null;
                }
                if (!isset($conversionConfig['multiplier'])) {
                    return null;
                }
                $value = number_format(
                    $order->getGrandTotal() * $conversionConfig['multiplier'],
                    2,
                    '.',
                    ','
                );
                return [
                    'currency' => $order->getOrderCurrency()->getCode(),
                    'send_to' => $conversionConfig['conversion_action_id'],
                    'transaction_id' => $order->getEntityId(),
                    'value' => $value,
                ];
            }, $this->getConversionConfigs())
        );
    }

    /**
     * Get target IDs
     *
     * @return array
     */
    public function getTargetIds()
    {
        return array_unique(
            array_filter(
                array_map(function ($conversionConfig) {
                    if (!isset($conversionConfig['target_id'])) {
                        return null;
                    }
                    return $conversionConfig['target_id'];
                }, $this->getConversionConfigs())
            )
        );
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }

    /**
     * Get conversion configs
     *
     * @return array
     */
    public function getConversionConfigs()
    {
        $websiteId = (int) $this->storeManager->getStore()->getWebsiteId();
        $conversionConfigsJson =
            $this->config->getConversionConfigsGoogle($websiteId) ?: '[]';
        return json_decode($conversionConfigsJson, true);
    }

    /**
     * Get order
     *
     * @return OrderInterface|null
     */
    private function getOrder()
    {
        $lastOrderId = $this->checkoutSession->getLastOrderId();
        if (!$lastOrderId) {
            return null;
        }
        return $this->orderRepository->get((int) $lastOrderId);
    }
}
