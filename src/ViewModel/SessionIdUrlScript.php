<?php

namespace Reaktion\Tracking\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Reaktion\Tracking\Model\Config;

/**
 * View model for session ID URL script
 */
class SessionIdUrlScript implements ArgumentInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @param Config $config
     * @param UrlInterface
     */
    public function __construct(Config $config, UrlInterface $url)
    {
        $this->config = $config;
        $this->url = $url;
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }

    /**
     * Get session ID script URL
     *
     * @return string
     */
    public function getSessionIdScriptUrl()
    {
        return $this->url->getUrl('rest/V1/reaktion/tracking/sessions');
    }
}
