<?php

namespace Reaktion\Tracking\ViewModel;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Reaktion\Tracking\Model\Config;

/**
 * View model for checkout script
 */
class CheckoutScript implements ArgumentInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @param Config $config
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Config $config,
        CheckoutSession $checkoutSession
    ) {
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Get checkout id
     *
     * @return int
     */
    public function getCheckoutId()
    {
        return (int) $this->checkoutSession->getQuoteId();
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isEnabled();
    }
}
