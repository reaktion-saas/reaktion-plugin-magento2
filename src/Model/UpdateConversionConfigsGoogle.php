<?php

namespace Reaktion\Tracking\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Reaktion\Tracking\Api\UpdateConversionConfigsGoogleInterface;

/**
 * Update conversion configs google via rest api
 */
class UpdateConversionConfigsGoogle implements
    UpdateConversionConfigsGoogleInterface
{
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @param WriterInterface $configWriter
     * @param WebsiteRepositoryInterface $websiteRepository
     */
    public function __construct(
        WriterInterface $configWriter,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->configWriter = $configWriter;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute(
        $conversionConfigsGoogle,
        $websiteId
    ) {
        $this->validateWebsiteId($websiteId);
        $this->configWriter->save(
            Config::XML_PATH_CONVERSION_CONFIGS_GOOGLE,
            $conversionConfigsGoogle,
            ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
        return true;
    }

    /**
     * @throws NoSuchEntityException
     * @param int $websiteId
     * @return void
     */
    private function validateWebsiteId($websiteId)
    {
        $this->websiteRepository->getById($websiteId);
    }
}
