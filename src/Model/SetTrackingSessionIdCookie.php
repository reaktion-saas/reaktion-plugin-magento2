<?php

namespace Reaktion\Tracking\Model;

use Magento\Framework\App\FrontController\Interceptor;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;

class SetTrackingSessionIdCookie
{
    const COOKIE_NAME = 'reaktion_session_id';
    const COOKIE_TTL = 2147483647;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var Random
     */
    private $random;

    /**
     * @param Config $config
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param Random $random
     */
    public function __construct(
        Config $config,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        Random $random
    ) {
        $this->config = $config;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->random = $random;
    }

    /**
     * @param Interceptor $interceptor
     * @return void
     */
    public function beforeDispatch(Interceptor $interceptor)
    {
        if ($this->config->getTrackingConsentPolicy() === 'ignore') {
            // Useful when testing the tracking features.
            $this->setCookie();
            return;
        }
        // Consent choices are made after the page is loaded in the browser.
        // Determining the consent choice on the server-side is hard to do.
        // Assume there's no consent and let the tracking script use the session endpoint when consent is given.
    }

    /**
     * @return void
     */
    public function setCookie()
    {
        if ($this->cookieManager->getCookie(static::COOKIE_NAME)) {
            return;
        }

        $sessionId = $this->generateSessionId();

        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration(static::COOKIE_TTL)
            ->setPath('/');

        $this->cookieManager->setPublicCookie(
            static::COOKIE_NAME,
            $sessionId,
            $metadata
        );
    }

    /** @return string */
    private function generateSessionId()
    {
        return $this->random->getRandomString(64);
    }
}
