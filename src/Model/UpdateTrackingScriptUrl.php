<?php

namespace Reaktion\Tracking\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Reaktion\Tracking\Api\UpdateTrackingScriptUrlInterface;

/**
 * Update tracking script URL via rest api
 */
class UpdateTrackingScriptUrl implements UpdateTrackingScriptUrlInterface
{
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @param WriterInterface $configWriter
     * @param WebsiteRepositoryInterface $websiteRepository
     */
    public function __construct(
        WriterInterface $configWriter,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->configWriter = $configWriter;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute($trackingScriptUrl, $websiteId)
    {
        $this->validateWebsiteId($websiteId);
        $this->configWriter->save(
            Config::XML_PATH_TRACKING_SCRIPT_URL,
            $trackingScriptUrl,
            ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
        return true;
    }

    /**
     * @throws NoSuchEntityException
     * @param int $websiteId
     * @return void
     */
    private function validateWebsiteId($websiteId)
    {
        $this->websiteRepository->getById($websiteId);
    }
}
