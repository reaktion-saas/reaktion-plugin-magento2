<?php

namespace Reaktion\Tracking\Model;

use Reaktion\Tracking\Api\CreateSessionIdInterface;

/**
 * Create session ID via rest api (public endpoint for frontend customers)
 */
class CreateSessionId implements CreateSessionIdInterface
{
    /**
     * @var SetTrackingSessionIdCookie
     */
    private $setTrackingSessionIdCookie;

    /**
     * @param SetTrackingSessionIdCookie $setTrackingSessionIdCookie
     */
    public function __construct(
        SetTrackingSessionIdCookie $setTrackingSessionIdCookie
    ) {
        $this->setTrackingSessionIdCookie = $setTrackingSessionIdCookie;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $this->setTrackingSessionIdCookie->setCookie();
    }
}
