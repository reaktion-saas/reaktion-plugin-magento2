<?php

namespace Reaktion\Tracking\Model;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Reaktion\Tracking\Api\UpdateTrackingConsentPolicyInterface;

/**
 * Update tracking consent policy via rest api
 */
class UpdateTrackingConsentPolicy implements
    UpdateTrackingConsentPolicyInterface
{
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @param WriterInterface $configWriter
     */
    public function __construct(WriterInterface $configWriter)
    {
        $this->configWriter = $configWriter;
    }

    /**
     * @inheritDoc
     */
    public function execute($trackingConsentPolicy)
    {
        $this->configWriter->save(
            Config::XML_PATH_TRACKING_CONSENT_POLICY,
            $trackingConsentPolicy
        );
        return true;
    }
}
