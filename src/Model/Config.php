<?php

namespace Reaktion\Tracking\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Module config
 */
class Config
{
    const XML_PATH_CONVERSION_CONFIGS_GOOGLE = 'reaktion_tracking/general/conversion_configs_google';
    const XML_PATH_ENABLED = 'reaktion_tracking/general/enabled';
    const XML_PATH_TRACKING_CONSENT_POLICY = 'reaktion_tracking/general/tracking_consent_policy';
    const XML_PATH_TRACKING_SCRIPT_URL = 'reaktion_tracking/general/tracking_script_url';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get conversion configs google
     *
     * @return string|null
     */
    public function getConversionConfigsGoogle($websiteId)
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_CONVERSION_CONFIGS_GOOGLE,
            ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
    }

    /**
     * Get tracking consent policy
     *
     * @return string|null
     */
    public function getTrackingConsentPolicy()
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_TRACKING_CONSENT_POLICY
        );
    }

    /**
     * Get tracking script url
     *
     * @return string|null
     */
    public function getTrackingScriptUrl($websiteId)
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_TRACKING_SCRIPT_URL,
            ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            static::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORES
        );
    }
}
