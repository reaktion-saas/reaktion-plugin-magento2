<?php

namespace Reaktion\Tracking\Api;

/**
 * Interface UpdateTrackingScriptUrlInterface
 */
interface UpdateTrackingScriptUrlInterface
{
    /**
     * Update settings
     *
     * @param string|null $trackingScriptUrl
     * @param int $websiteId
     * @return bool
     */
    public function execute($trackingScriptUrl, $websiteId);
}
