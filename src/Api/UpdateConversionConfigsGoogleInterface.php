<?php

namespace Reaktion\Tracking\Api;

/**
 * Interface UpdateConversionConfigsGoogleInterface
 */
interface UpdateConversionConfigsGoogleInterface
{
    /**
     * Update settings
     *
     * @param string|null $conversionConfigsGoogle JSON configuration
     * @param int $websiteId
     * @return bool
     */
    public function execute($conversionConfigsGoogle, $websiteId);
}
