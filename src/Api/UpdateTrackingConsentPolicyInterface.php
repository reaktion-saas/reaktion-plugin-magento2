<?php

namespace Reaktion\Tracking\Api;

/**
 * Interface UpdateTrackingConsentPolicyInterface
 */
interface UpdateTrackingConsentPolicyInterface
{
    /**
     * Update settings
     *
     * @param string|null $trackingConsentPolicy
     * @return bool
     */
    public function execute($trackingConsentPolicy);
}
