<?php

namespace Reaktion\Tracking\Api;

/**
 * Interface CreateSessionIdInterface
 */
interface CreateSessionIdInterface
{
    /**
     * Create session ID
     *
     * @return void
     */
    public function execute();
}
