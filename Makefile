.PHONY: build clean dev magento2

build:
	infra/bin/build.sh

clean:
	infra/bin/clean.sh

dev:
	infra/bin/dev.sh

magento2:
	infra/bin/magento2.sh
