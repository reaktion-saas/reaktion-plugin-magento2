#!/usr/bin/env bash

set -euxo pipefail

USER_ID=$(id -u) GROUP_ID=$(id -g) docker compose -f infra/docker-compose.magento2.yml up -d --build
rm -fr magento2
docker cp infra-reaktion-plugin-magento2-magento2-1:/app magento2
docker rm -f infra-reaktion-plugin-magento2-magento2-1
