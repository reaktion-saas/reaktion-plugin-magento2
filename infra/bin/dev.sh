#!/usr/bin/env bash

set -euxo pipefail

docker compose -f infra/docker-compose.dev.yml up -d --build
docker exec -ti reaktion-plugin-magento2-dev-fpm sh -c '
    composer install \
    && bin/magento setup:install \
        --admin-firstname=Admin \
        --admin-lastname=Admin \
        --admin-email=admin@reaktion.com \
        --admin-user=admin \
        --admin-password=admin123 \
        --backend-frontname=admin \
        --currency=DKK \
        --db-host=reaktion-plugin-magento2-dev-database \
        --db-name=reaktion \
        --db-password=secret \
        --db-user=reaktion \
        --base-url=http://localhost:8080 \
        --base-url-secure=0 \
        --language=en_GB \
        --opensearch-host=reaktion-plugin-magento2-dev-opensearch\
        --opensearch-port=9200 \
        --opensearch-index-prefix=magento2 \
        --opensearch-timeout=15 \
        --opensearch-enable-auth=false \
        --search-engine=opensearch \
        --timezone=Europe/Copenhagen \
        --use-secure=0 \
        --use-secure-admin=0 \
        && bin/magento setup:upgrade \
        && bin/magento setup:di:compile \
        && bin/magento setup:static-content:deploy -f \
        && bin/magento indexer:reindex \
        && bin/magento cache:flush
'