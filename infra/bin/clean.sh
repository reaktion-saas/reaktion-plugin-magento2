#!/usr/bin/env bash

set -euxo pipefail

docker compose -f infra/docker-compose.dev.yml down || true
